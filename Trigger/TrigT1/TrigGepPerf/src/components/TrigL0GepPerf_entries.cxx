#include "../GepClusteringAlg.h"
DECLARE_COMPONENT( GepClusteringAlg )

#include "../GepJetAlg.h"
DECLARE_COMPONENT( GepJetAlg )

#include "../GepMETAlg.h"
DECLARE_COMPONENT( GepMETAlg )

#include "../GepMETPufitAlg.h"
DECLARE_COMPONENT( GepMETPufitAlg )

#include "../GepClusterTimingAlg.h"
DECLARE_COMPONENT( GepClusterTimingAlg )

#include "../CaloCellsHandlerTool.h"
DECLARE_COMPONENT( CaloCellsHandlerTool )

